{
  const find = () => ({
    fromCollection: collection => ({
      withSelector: (selector = {}) =>
        db.getCollection(collection).find(selector),
    }),
  });

  const mapObject = (object, iteratee = item => object[item]) =>
    Object.keys(object).map(iteratee);

  const collectionName = 'someCollection';
  const selector = { aField: 'yes' };

  mapObject(
    find()
      .fromCollection(collectionName)
      .withSelector(selector)
      .map(({ errorMessage }) => errorMessage)
      .reduce((acc, cur) => {
        let count = 1;

        if (acc[cur]) {
          count += acc[cur].count;
        }

        return Object.assign({}, acc, {
          [cur]: { errorMessage: cur, count },
        });
      }, {}),
  );
}
